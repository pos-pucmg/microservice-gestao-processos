package com.indtexbr.sigo.processosindustriais.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

//@lombok.Data
@Entity
@Table(name="T_ALERTA")
public class Alerta {
    @Id
    @Column(name="ID_ALERTA")
    private Long idAlerta;

    @Column(name="ID_PROCESSO")
    private Long idProcesso;

    @Column(name="IN_SEVERIDADE")
    private Integer severidade;

    @Column(name="TX_DESCRICAO")
    private String descricao;

    @Column(name="DT_ALERTA")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "UTC")
    private Date dataOcorrencia;

    public Alerta() {
    }

    public Long getIdAlerta() {
        return idAlerta;
    }

    public void setIdAlerta(Long idAlerta) {
        this.idAlerta = idAlerta;
    }

    public Long getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(Long idProcesso) {
        this.idProcesso = idProcesso;
    }

    public Integer getSeveridade() {
        return severidade;
    }

    public void setSeveridade(Integer severidade) {
        this.severidade = severidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataOcorrencia() {
        return dataOcorrencia;
    }

    public void setDataOcorrencia(Date dataOcorrencia) {
        this.dataOcorrencia = dataOcorrencia;
    }
}
