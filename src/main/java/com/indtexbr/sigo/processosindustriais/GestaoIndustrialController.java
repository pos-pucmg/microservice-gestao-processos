/*
 * Copyright 2018-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.indtexbr.sigo.processosindustriais;

import com.indtexbr.sigo.processosindustriais.models.Alerta;
import com.indtexbr.sigo.processosindustriais.models.Processo;
import com.indtexbr.sigo.processosindustriais.repositories.AlertaRepository;
import com.indtexbr.sigo.processosindustriais.repositories.ProcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Gary Russell
 * @since 2.2.1
 *
 */

@CrossOrigin
@RestController
@RequestMapping(value="/sigo/mgpi")
public class GestaoIndustrialController {

	@Autowired
	AlertaRepository alertaRepository;
	@Autowired
	ProcessoRepository processoRepository;

	@GetMapping(value="/processos", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<List<Processo>> buscarProcessos() {
		List<Processo> listaProcessos = StreamSupport
				.stream(processoRepository.findAll().spliterator(), false)
				.collect(Collectors.toList());
		if(CollectionUtils.isEmpty(listaProcessos)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(listaProcessos, HttpStatus.OK);
	}

	@GetMapping(value="/alertas", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<List<Alerta>> buscarAlertas() {
		List<Alerta> listaAlertas = StreamSupport
				.stream(alertaRepository.findAll().spliterator(), false)
				.collect(Collectors.toList());
		if(CollectionUtils.isEmpty(listaAlertas)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(listaAlertas, HttpStatus.OK);
	}

}
