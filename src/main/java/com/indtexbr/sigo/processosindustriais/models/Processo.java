package com.indtexbr.sigo.processosindustriais.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

//@lombok.Data
@Entity
@Table(name="T_PROCESSO")
public class Processo {
    @Id
    @Column(name="ID_PROCESSO")
    private Long id;
    @Column(name="IN_AREA_NEGOCIO")
    private String idAreaNegocio;
    @Column(name="ST_PROCESSO")
    private String status;
    @Column(name="DT_STATUS")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "UTC")
    private Date dataStatus;
    @Column(name="TX_PROCESSO")
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdAreaNegocio() {
        return idAreaNegocio;
    }

    public void setIdAreaNegocio(String idAreaNegocio) {
        this.idAreaNegocio = idAreaNegocio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Date dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
