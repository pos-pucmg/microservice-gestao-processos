package com.indtexbr.sigo.processosindustriais.repositories;

import com.indtexbr.sigo.processosindustriais.models.Processo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessoRepository extends CrudRepository<Processo,Long> {

}
