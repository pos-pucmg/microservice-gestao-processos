package com.indtexbr.sigo.processosindustriais.repositories;

import com.indtexbr.sigo.processosindustriais.models.Alerta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertaRepository extends CrudRepository<Alerta,Long> {

}
