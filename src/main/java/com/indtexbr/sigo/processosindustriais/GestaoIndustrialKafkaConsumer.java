/*
 * Copyright 2018-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.indtexbr.sigo.processosindustriais;

import com.indtexbr.sigo.processosindustriais.models.Alerta;
import com.indtexbr.sigo.processosindustriais.models.Processo;
import com.indtexbr.sigo.processosindustriais.repositories.AlertaRepository;
import com.indtexbr.sigo.processosindustriais.repositories.ProcessoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author Gary Russell
 * @since 5.1
 *
 */
@Component
@KafkaListener(id = "gestaoIndustrial", topics = { "processos", "alertas" })
public class GestaoIndustrialKafkaConsumer {

	@Autowired
	AlertaRepository alertaRepository;
	@Autowired
	ProcessoRepository processoRepository;

	private static final Logger logger = LogManager.getLogger("GestaoIndustrialKafkaConsumer");

	@KafkaHandler
	public void processo(Processo p) {
		logger.info(p.toString());
		processoRepository.save(p);
	}

	@KafkaHandler
	public void alerta(Alerta a) {
		alertaRepository.save(a);
	}

	@KafkaHandler(isDefault = true)
	public void unknown(Object object) {
		logger.warn(object);
	}

}
